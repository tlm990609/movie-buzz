# Docker Environment for TLM's Lesson 10

## Purpose
This Docker environment is designed to provide a consistent environment for the
lesson 10 of the TLM course. It is quick to set up, easy to use, and clean up.

## Prerequisites
- Docker installed on your machine.

## Setup
1. Clone this repository to your local machine;
2. Expand the `movie-buzz.zip` file in the `api` directory (at the same level as the startup directory);
3. Open a terminal and navigate to the root of the repository;
4. Run the following command to build the Docker image:
   ```
   docker-compose up
   ```
5. Once the image is built and running, you can access the application at `http://localhost:4000`
6. You can also use Visual Studio Code with a MySQL extension to connect to the database. The connection details are:
   - Host: `mongodb://127.0.0.1`

### Notes
- The startup process will take a few minutes to complete, especially the first time you run it;
- The startup process will seed the database with some initial data.

## Docker Environment
The following picture shows the Docker environment for this setup.

[![Docker Environment](./environment.png)](./environment.png)
