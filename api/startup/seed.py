from pymongo import MongoClient
import json
import os
import re

# Adjust the connection sting to use the hostname on the MongoDB container.
# If MONGO_URI is not set, use the default hostname, otherwise use the value
# from MONGO_URI to change the connectionString constant on file
# config/db_connection.js
if "MONGO_URI" in os.environ:
    print("Updating the connection string in config/db_connection.js")
    connectionString = os.environ["MONGO_URI"]
    with open("config/db_connection.js", "r") as file:
        data = file.read()
        if "MONGO_URI" not in data:
            # Use regex to replace the old connection string with the new one
            data = re.sub(
                r"const connectionString = .*",
                f'const connectionString = "{connectionString}"',
                data,
            )
            with open("config/db_connection.js", "w") as file_out:
                file_out.write(data)

    # Connect to MongoDB and seed the database
    with MongoClient("mongodb://mongo") as client:
        # Select the database
        db = client["movie-buzz"]
        if "movies" not in db.list_collection_names():
            # Select the MongoDB collection
            print("creating collection")
            collection = db["movies"]
            # Load data from JSON file
            with open("db_seed/movies.json") as f:
                data = f.read()
                # count the amount of lines starting with a single } using regex
                count = len(re.findall(r"}\n", data))
                # replace every line starting with a single } with a single }, except the last line
                data = re.sub(r"}\n", "},\n", data, count - 1)
                # add square brackets to the beginning and end of the file
                data = "[" + data + "]"
                jdata = json.loads(data)
                for ii in jdata:
                    del(ii['_id'])
                    collection.insert_one(ii)
            print("Data loaded from JSON file and inserted into MongoDB successfully!")
